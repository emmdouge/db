package runner;
import java.util.ArrayList;

import org.junit.experimental.ParallelComputer;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import com.mysql.jdbc.Util;

import domainobj.ITEM_TYPE;
import domainobj.InventoryItem;
import domainobj.Nail;
import domainobj.PowerTool;
import domainobj.StripNails;
import domainobj.Tool;
import finder.InventoryFinder;
import gateway.InventoryItemGateway;
import test.AllObjectsTest;
import test.AllTests;

public class Runner {

	public static void main(String[] args) {
	    Result result = JUnitCore.runClasses(ParallelComputer.methods(), AllTests.class);
	    for (Failure failure : result.getFailures()) {
          System.out.println(failure.toString());
       }
       System.out.println(result.wasSuccessful());
	}

	public static ArrayList<InventoryItem> createInventoryItems(ArrayList<InventoryItemGateway> gatewayList) {
		ArrayList<InventoryItem> list = new ArrayList<InventoryItem>();
		for(InventoryItemGateway gateway: gatewayList) {
			if(gateway.getType() == ITEM_TYPE.TOOL.ordinal()) {
				list.add(Runner.createTool(gateway));
			}
			else if(gateway.getType() == ITEM_TYPE.POWERTOOl.ordinal()) {
				list.add(Runner.createPowerTool(gateway));
			}		
		}
		return list;
	}
	
	public static Tool createTool(InventoryItemGateway gateway) {
		return new Tool(gateway.getId(), gateway.getUPC(), gateway.getManufacturerId(), gateway.getPrice(), gateway.getDescription());
	}
	
	public static PowerTool createPowerTool(InventoryItemGateway gateway) {
		return new PowerTool(gateway.getId(), gateway.getUPC(), gateway.getManufacturerId(), gateway.getPrice(), gateway.getDescription(), gateway.getBatteryPowered());
	}
	
	public static StripNails createStripNails(InventoryItemGateway gateway) {
		return new StripNails(gateway.getId(), gateway.getUPC(), gateway.getManufacturerId(), gateway.getPrice(), gateway.getLength(), gateway.getNumberInStrip());
	}
	
	public static Nail createNail(InventoryItemGateway gateway) {
		return new Nail(gateway.getId(), gateway.getUPC(), gateway.getManufacturerId(), gateway.getPrice(), gateway.getLength(), gateway.getNumberInBox());
	}
}
