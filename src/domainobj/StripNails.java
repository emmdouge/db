package domainobj;

public class StripNails extends Fastener {
	private int numberInStrip;
	
	public StripNails(int id, String upc, int manufacturerId, int price, double length, int numberInStrip) {
		super(id, upc,manufacturerId, price, length);
		this.numberInStrip = numberInStrip;
	}
}
