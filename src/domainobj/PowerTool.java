package domainobj;

public class PowerTool extends Tool {
	private boolean batteryPowered;
	
	public PowerTool(int id, String upc, int manufacturerId, int price, String description, boolean batteryPowered) {
		super(id, upc, manufacturerId, price, description);
		this.batteryPowered = batteryPowered;
	}
}
