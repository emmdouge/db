package domainobj;

public abstract class InventoryItem {
	private int id;
	private String upc;
	private int manufacturerId;
	private int price;
	
	public void Inventoryitem(int id, String upc, int manufacturerId, int price) {
		this.id = id;
		this.upc = upc;
		this.manufacturerId = manufacturerId;
		this.price = price;
	}
	
	public int getId() {
		return this.id;
	}
	
	public String getUPC() {
		return this.upc;
	}
	
	public int getManufacturerId() {
		return this.manufacturerId;
	}
	
	public int getPrice() {
		return this.price;
	}
}
