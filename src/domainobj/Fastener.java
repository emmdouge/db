package domainobj;

public abstract class Fastener extends InventoryItem {
	private double length;
	
	public Fastener(int id, String upc, int manufacturerId, int price, double length) {
		super.Inventoryitem(id, upc, manufacturerId, price);
		this.length = length;
	}
}
