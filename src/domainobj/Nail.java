package domainobj;

public class Nail extends Fastener {
	private int numberInBox;
	
	public Nail(int id, String upc, int manufacturerId, int price, double length, int numberInBox) {
		super(id, upc,manufacturerId, price, length);
		this.numberInBox = numberInBox;
	}
}