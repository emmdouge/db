package domainobj;

public class Tool extends InventoryItem {
	private String  description;
	
	public Tool(int id, String upc, int manufacturerId, int price, String description) {
		super.Inventoryitem(id, upc, manufacturerId, price);
		this.description = description;
	}
	
	public String getDesciprtion() {
		return this.description;
	}
}
