package gateway;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class InventoryItemGateway {
	
	private int id;
	private String upc;
	private int manufacturerId;
	private int price;
	private double length;
	private int numberInBox;
	private int numberInStrip;
	private String description;
	private boolean batteryPowered;
	private int toPowerTool;
	private int stripUsed;
	private int type;
	
	public InventoryItemGateway(int id) {
		try{  
			System.out.println("connecting");
			Class.forName("com.mysql.jdbc.Driver");  
			Connection con = DriverManager.getConnection("jdbc:mysql://db.cs.ship.edu/swe400-22","swe400_2","pwd4swe400_2F16");
			Statement stmt = con.createStatement();  
			ResultSet rs = stmt.executeQuery("SELECT * from InventoryItem WHERE Id = "+id);
			if (rs.next()) {
				System.out.println("here!");
				this.id = rs.getInt("Id");
				this.upc = rs.getString("UPC");
				this.manufacturerId = rs.getInt("ManufacturerId");
				this.price = rs.getInt("Price");
				this.length = rs.getDouble("Length");
				this.numberInBox = rs.getInt("NumberInBox");
				this.numberInStrip = rs.getInt("NumberInStrip");
				this.description = rs.getString("Description");
				this.toPowerTool = rs.getInt("ToPowerTool");
				this.stripUsed = rs.getInt("StripUsed");
			} 
		}
		catch(Exception e){ System.out.println(e); } 
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setUPC(String upc) {
		this.upc = upc;
	}
	
	public void setManufacturerId(int manufacturerId) {
		this.manufacturerId = manufacturerId;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public void setLength(double length) {
		this.length = length;
	}
	
	public void setNumberInBox(int numberInBox) {
		this.numberInBox = numberInBox;
	}
	
	public void setNumberInStrip(int numberInStrip) {
		this.numberInStrip = numberInStrip;
	}
	
	public void setDescription(String desc) {
		this.description = desc;
	}
	
	public void setBatteryPowered(boolean batteryPowered) {
		this.batteryPowered = batteryPowered;
	}
	
	public void setToPowerTool(int toPowerTool) {
		this.toPowerTool = toPowerTool;
	}
	
	public void setStripUsed(int stripUsed) {
		this.stripUsed = stripUsed;
	}
	
	public void setType(int type) {
		this.type = type;
	}

	public int getId() {
		return this.id;
	}
	
	public String getUPC() {
		return this.upc;
	}
	
	public int getManufacturerId() {
		return this.manufacturerId;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public double getLength() {
		return this.length;
	}
	
	public int getNumberInBox() {
		return this.numberInBox;
	}
	
	public int getNumberInStrip() {
		return this.numberInStrip;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public boolean getBatteryPowered() {
		return this.batteryPowered;
	}
	
	public int getToPowerTool() {
		return this.toPowerTool;
	}
	
	public int getStripUsed() {
		return this.stripUsed;
	}
	
	public int getType() {
		return this.type;
	}
	
	public void insert(String upc, int manufacturerId, int price, double length, int numberInBox, String description, boolean batteryPowered) {
		
	}
}
