package test;

import static org.junit.Assert.*;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import domainobj.InventoryItem;
import domainobj.PowerTool;
import domainobj.Tool;
import finder.InventoryFinder;
import gateway.InventoryItemGateway;
import runner.Runner;

public class AllObjectsTest {

	@Test
	public void testFindById() {
	    InventoryFinder invFinder = new InventoryFinder();
		InventoryFinder.initIdentityMap();
		ArrayList<InventoryItemGateway> items = invFinder.findById(2);
		assertEquals(items.get(0).getId(), 2);
	}
	
	@Test
	public void testFindAll() {
	    InventoryFinder invFinder = new InventoryFinder();
		InventoryFinder.initIdentityMap();
		ArrayList<InventoryItemGateway> items = (invFinder.findAll());
		items.sort(Comparator.comparingInt(InventoryItemGateway::getId));
//		Tool tool = new Tool(2, "123456789012", 12345, 9, "Ballpoint Hammer");
//		PowerTool powerTool = new PowerTool(3, "123456789013", 12346, 40, "Cordless Drill", true);

		ArrayList<InventoryItemGateway> testList = new ArrayList<InventoryItemGateway>();
//		testList.
//		
//		
//		for(int i = 0; i < items.size(); i++) {
//			assertEquals(items.get(i)., testList.get(i));
//		}
	}

}
