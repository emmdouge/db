package finder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import domainobj.ITEM_TYPE;
import domainobj.InventoryItem;
import domainobj.Nail;
import domainobj.PowerTool;
import domainobj.StripNails;
import domainobj.Tool;
import gateway.InventoryItemGateway;

public class InventoryFinder {
	private static Map<Integer, InventoryItemGateway> identityMap;
	public InventoryFinder() {}
	
	public static void initIdentityMap() {
		identityMap = new HashMap<Integer, InventoryItemGateway>();
	}
	
	public Map getIdentityMap() {
		return this.identityMap;
	}
	
	public ArrayList<InventoryItemGateway> findById(int id) {
		ArrayList<InventoryItemGateway> gatewayList = new ArrayList<InventoryItemGateway>();
		InventoryItemGateway gateway = InventoryFinder.identityMap.get(id);
		if(gateway == null) {
			System.out.println("not found");
			gateway = new InventoryItemGateway(id);
			InventoryFinder.identityMap.put(id, gateway);
		}
		gatewayList.add(gateway);
		return gatewayList;
	}
	
	public ArrayList<InventoryItemGateway> findAll() {
		try {
			Class.forName("com.mysql.jdbc.Driver");  
			Connection con = DriverManager.getConnection("jdbc:mysql://db.cs.ship.edu/swe400-22","swe400_2","pwd4swe400_2F16");
			Statement stmt = con.createStatement();  
			ResultSet rs = stmt.executeQuery("SELECT Id from InventoryItem");
			InventoryItemGateway gateway = null;
			ArrayList<InventoryItemGateway> list = new ArrayList<InventoryItemGateway>();
			while (rs.next()) {
				gateway = this.findById(rs.getInt("Id")).get(0);
				list.add(gateway);
			} 
			return list;
		}
		catch(Exception e){ System.out.println(e); } 
		return null;
	}
}
